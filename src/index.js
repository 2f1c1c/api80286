import express from 'express';
import fetch from 'isomorphic-fetch';
const _ = require('lodash');

const app = express();


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/volumes', async function(req, res) {
  let pcInfo = await getJson();	
  let a = {};
  for (let value of pcInfo.hdd) {
	  a[value.volume] = _.sumBy(_.filter(pcInfo.hdd, { volume: value.volume }), 'size') + 'B';
  }
  res.json(a);
 
});


app.get('/', async (req,res) =>  {
	let pcInfo = await getJson();
	res.json(pcInfo);

});


app.get('/*', async function(req, res) {
	let pcInfo = await getJson();
	console.log(pcInfo);
	let pathString = req.params[0];
	let response = objFind(pcInfo, pathString);

	console.log(response);
	if (response === undefined ) {
		res.status(404).send('Not Found');
	} else {
		console.log("resp: " + response);
	res.json(response);
	}

});


app.listen(3000, function() {
});

async function getJson() {

	const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
	
	let pc = await fetch(pcUrl).then(async (res) => {
  	  return res.json();
  	}).catch(err => {
    	console.log('Something wrong:', err);
  	});
  	
	return pc;

}


function objFind (obj, pathString) {

	if (pathString.length === pathString.lastIndexOf('/')+1) {
		pathString = pathString.slice(0, pathString.length -1);
	}

	let pathArray = pathString.split('/');

  var current = obj;
	try {
 	for (let i = 0; i < pathArray.length; ++i) {
 
		let type = typeof current;

		if (current[pathArray[i]] === undefined) {
			return undefined;
		} else if (type === 'object') {
			current = current[pathArray[i]];
		} else {
			return undefined;
		}
	}
		return current;
  } catch(err) {
		return undefined;
  }
}
